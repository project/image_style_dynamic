<?php

namespace Drupal\image_style_dynamic\Routing;

use Drupal\Core\ParamConverter\ParamConverterInterface;
use Drupal\image\Entity\ImageStyle;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Route;

/**
 * Provides upcasting for an image style - load entity if found or pass string
 * for dynamic styles.
 */
class DynamicImageStyleConverter implements ParamConverterInterface {

  /**
   * {@inheritdoc}
   */
  public function convert($value, $definition, $name, array $defaults) {
    if ($image_style_object = ImageStyle::load($value)) {
      return $image_style_object;
    }
    elseif (strpos($value, '[') !== FALSE) {
      if ($allowed_image_styles = \Drupal::config('image_style_dynamic.settings')
        ->get('allowed_image_styles')) {
        $allowed = FALSE;
        foreach ($allowed_image_styles as $allowed_image_style) {
          if (($allowed_image_style[0] === '^' && preg_match("%$allowed_image_style%", $value))
            || ($allowed_image_style === $value)
          ) {
            $allowed = TRUE;
            break;
          }
        }
        if (!$allowed) {
          throw new AccessDeniedHttpException('Invalid image style: ' . $value);
        }
      }

      parse_str($value, $image_style_query);
      $image_style_object = ImageStyle::create([
        'name' => $value,
      ]);

      foreach ($image_style_query as $key => $configuration) {
        $image_style_object->addImageEffect([
          'id' => $key,
          'data' => $configuration,
        ]);
      }

      return $image_style_object;
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function applies($definition, $name, Route $route) {
    return isset($definition['type']) && $definition['type'] == 'image_style_dynamic';
  }
}
